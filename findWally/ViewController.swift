import UIKit
import AVFoundation

var image : UIImage?

class ViewController: UIViewController {
    
    // デバイスからの入力と出力を管理するオブジェクトの作成
    var captureSession = AVCaptureSession()
    // カメラデバイスそのものを管理するオブジェクトの作成
    // メインカメラの管理オブジェクトの作成
    var mainCamera: AVCaptureDevice?
    // インカメの管理オブジェクトの作成
    var innerCamera: AVCaptureDevice?
    // 現在使用しているカメラデバイスの管理オブジェクトの作成
    var currentDevice: AVCaptureDevice?
    // キャプチャーの出力データを受け付けるオブジェクト
    var photoOutput : AVCapturePhotoOutput?
    // プレビュー表示用のレイヤ
    var cameraPreviewLayer : AVCaptureVideoPreviewLayer?
    //プレビュー表示用画像データ
    
    //非同期処理用変数
    var isGet = false
    
    // シャッターボタン
    @IBOutlet weak var cameraButton: UIButton!
    
    @IBAction func buttonTapped(_ sender : UIButton) {
        performSegue(withIdentifier: "toViewController2",sender: nil)

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCaptureSession()
        setupDevice()
        setupInputOutput()
       setupPreviewLayer()
        captureSession.startRunning()
        styleCaptureButton()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // シャッターボタンが押された時のアクション
    @IBAction func cameraButton_TouchUpInside(_ sender : UIButton) {
        let settings = AVCapturePhotoSettings()
        // フラッシュの設定
        settings.flashMode = .auto
        // カメラの手ぶれ補正
        settings.isAutoStillImageStabilizationEnabled = true
        // 撮影された画像をdelegateメソッドで処理
        self.photoOutput?.capturePhoto(with: settings, delegate: self as AVCapturePhotoCaptureDelegate)
        //画像をグローバル変数
        //画面遷移
        wait({!self.isGet}){
            let storyboard: UIStoryboard = self.storyboard!
            let preview = storyboard.instantiateViewController(withIdentifier: "preview" )
            self.present(preview, animated: true, completion: nil)
        }
    }
    // Segue 準備
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //        if (segue.identifier == "nextSegue") {
    //            let vcA: ViewControllerA = (segue.destination as? ViewControllerA)!
    //            // ViewControllerのvcAにイメージを設定
    //            self.wait({!self.isGet}) {
    //                vcA.image = self.getImage()
    //            }
    //
    //
    //        }
    //    }
    //
}

//MARK: AVCapturePhotoCaptureDelegateデリゲートメソッド
extension ViewController: AVCapturePhotoCaptureDelegate{
    // 撮影した画像データが生成されたときに呼び出されるデリゲートメソッド
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let imageData = photo.fileDataRepresentation() {
            // Data型をUIImageオブジェクトに変換
            let uiImage = UIImage(data: imageData)
            let uiImageResize = self.resizeImage(image: uiImage!, targetSize: CGSize(width: 480, height: 640) )
            
            // UIImageView 初期化
            let imageView = UIImageView(image:uiImageResize)
            
            // 画面の横幅を取得
//            let screenWidth:CGFloat = view.frame.size.width
//            let screenHeight:CGFloat = view.frame.size.height
            let screenWidth:CGFloat = view.frame.size.width
            let screenHeight:CGFloat = view.frame.size.height
            // 画像の中心を画面の中心に設定
            imageView.center = CGPoint(x:screenWidth/2, y:screenHeight/2)
            
            // UIImageViewのインスタンスをビューに追加
            self.view.addSubview(imageView)
            // 写真ライブラリに画像を保存
            UIImageWriteToSavedPhotosAlbum(uiImageResize, nil,nil,nil)
            func preareForSegue() {
                
            }
            //画像データ一時保存
            setImage(imageP: uiImageResize)
            
        }
    }
    
}








//MARK: カメラ設定メソッド
extension ViewController{
    // カメラの画質の設定
    func setupCaptureSession() {
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        
    }
    
    // デバイスの設定
    func setupDevice() {
        // カメラデバイスのプロパティ設定
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        // プロパティの条件を満たしたカメラデバイスの取得
        let devices = deviceDiscoverySession.devices
        
        for device in devices {
            if device.position == AVCaptureDevice.Position.back {
                mainCamera = device
            } else if device.position == AVCaptureDevice.Position.front {
                innerCamera = device
            }
        }
        // 起動時のカメラを設定
        currentDevice = mainCamera
    }
    
    // 入出力データの設定
    func setupInputOutput() {
        do {
            // 指定したデバイスを使用するために入力を初期化
            let captureDeviceInput = try AVCaptureDeviceInput(device: currentDevice!)
            // 指定した入力をセッションに追加
            captureSession.addInput(captureDeviceInput)
            // 出力データを受け取るオブジェクトの作成
            photoOutput = AVCapturePhotoOutput()
            // 出力ファイルのフォーマットを指定
            photoOutput!.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey : AVVideoCodecType.jpeg])], completionHandler: nil)
            captureSession.addOutput(photoOutput!)
        } catch {
            print(error)
        }
    }
    
    // カメラのプレビューを表示するレイヤの設定
    func setupPreviewLayer() {
        // 指定したAVCaptureSessionでプレビューレイヤを初期化
        self.cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        // プレビューレイヤが、カメラのキャプチャーを縦横比を維持した状態で、表示するように設定
        self.cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        // プレビューレイヤの表示の向きを設定
        self.cameraPreviewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
        
        self.cameraPreviewLayer?.frame = CGRect(x: 0, y: 129, width: 375 ,height: 554)
        self.view.layer.insertSublayer(self.cameraPreviewLayer!, at: 0)
    }
    
    //---setter,getter
    private func setImage(imageP: UIImage){
        image = imageP
        isGet = true
        
    }
    
    private func getImage() -> UIImage{
        return image!
    }
    
    
    //---非同期処理
    private func wait(_ waitContinuation: @escaping (() -> Bool), compleation: @escaping (() -> Void)) {
        
        var wait = waitContinuation()
        
        // Wait for the wait condition to be cleared at 0.01 second interval.
        
        let semaphore = DispatchSemaphore(value: 0)
        DispatchQueue.global().async {
            while wait {
                DispatchQueue.main.async {
                    wait = waitContinuation()
                    semaphore.signal()
                }
                
                semaphore.wait()
                Thread.sleep(forTimeInterval: 0.01)
            }
            
            DispatchQueue.main.async {
                compleation()
            }
        }
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    
//     ボタンのスタイルを設定
        func styleCaptureButton() {
            let deg = 90.0
//            cameraButton.layer.borderColor = UIColor.blue.cgColor
            cameraButton.layer.borderWidth = 5
            cameraButton.clipsToBounds = true
            cameraButton.layer.cornerRadius = min(cameraButton.frame.width, cameraButton.frame.height) / 2
            cameraButton.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI*deg/180.0))
        }
}
