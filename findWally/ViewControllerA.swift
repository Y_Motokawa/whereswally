import UIKit
import Foundation
import Alamofire
import SwiftyJSON
import AssetsLibrary
import SpriteKit

var detectionBoxes: [[Double]] = [[]]
var classNames:[String] = []
var detectionScores:[Double] = []
var num_detections:Int = 0
var screenWidthG:Double = 0
var screenHeightG:Double = 0
var activityIndicatorView = UIActivityIndicatorView()
class ViewControllerA: UIViewController{
    
    
    var isGet = false
    var isGet2 = false
    var isGet3 = false
    var isGet4 = false
    var bearerToken = ""
    var xMin = 0
    var xMax = 0
    var yMin = 0
    var yMax = 0
    @IBOutlet weak var Button: UIButton!
    
//    @IBOutlet weak var textView: UITextView!
    
    
//    @IBAction func back(_ sender: Any) {
//        dismiss(animated: true, completion: nil)
//    }
    //ViewControllerから受け渡した画像データ
    @IBOutlet var imageView: UIImageView!
    //    var image : UIImage!
    
    
    //---メイン処理
    override func viewDidLoad() {
        super.viewDidLoad()
        //初期設定
        styleCaptureButton()
        //ActivityView
        activityView()
        //撮影画面描画処理
        imagePreview()
        //認証処理
        let bearerToken: String = authorizasion()
        //ObjectDetection処理
        self.wait({!self.isGet}){
            self.UPLOD(bearerToken: bearerToken)
        }
        //Rectangle描画処理
        self.wait({!self.isGet4}){
            self.drawRectangle()
           
            
            
        }
        // Screen Size の取得
        
    }
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //
    //            let vc:Draw = (segue.destination as? Draw)!
    //            // ViewControllerのvcAにイメージを設定
    //            self.wait({!self.isGet3}) {
    //               vc.detectionBoxes = self.getDetectionBoxes()
    //                vc.detectionScores = self.getDetectionScores()
    //                vc.classNames = self.getClassNames()
    //                vc.num_detections = self.getNum_detections()
    //            }
    //
    //
    //        }
    
    
    //---ActivityView
    func activityView(){
        activityIndicatorView.center = view.center
        activityIndicatorView.style = .whiteLarge
        activityIndicatorView.color = .white
            view.addSubview(activityIndicatorView)
        // アニメーション開始
        activityIndicatorView.startAnimating()

    }
    
    
    //---撮影画面描画処理
    func imagePreview() {
        
         imageView.image = image!
//        let image2:UIImage = UIImage(named:"43_10")!
//       imageView.image = image2
//        imageView.sendSubviewToBack(imageView)
        
//        self.textView.text = "aaaaaa"
        // テキストのフォントを設定
//        self.textView.font = UIFont.systemFont(ofSize: 12)
//        self.textView.textColor = UIColor.red
//        self.view.addSubview(self.textView)
        
    }
    
    
    //---認証処理
    func authorizasion() -> String {
        let user = "sb-acbb5564-d001-4211-83d4-b4edcde7b14d!b5084|ml-foundation-xsuaa-std!b540"
        let password = "u0kvmH0geZY/kpniNa7ff5sUNII="
        let headers: HTTPHeaders = [
            "Authorization": "Basic c2ItYWNiYjU1NjQtZDAwMS00MjExLTgzZDQtYjRlZGNkZTdiMTRkIWI1MDg0fG1sLWZvdW5kYXRpb24teHN1YWEtc3RkIWI1NDA6dTBrdm1IMGdlWlkva3BuaU5hN2ZmNXNVTklJPQ==",
            "Accept": "application/json",
            "tenantName": "jsol-newsol",
            "cache-control": "no-cache",
            "Postman-Token": "f4ef4e56-58c0-438f-8cb9-584cab48f509"
        ]
        
        Alamofire.request("https://jsol-newsol.authentication.eu10.hana.ondemand.com/oauth/token?grant_type=client_credentials", headers: headers).responseJSON { response in
            self.isGet = true
            self.wait({!self.isGet}){
                switch response.result {
                case .success:
                    let json:JSON = JSON(response.result.value ?? kill)
                    let jsonString = json["access_token"].string
                    let bearerToken: String  = "Bearer " + jsonString!
                    self.setBearerToken(bearerToken: bearerToken)
                    
                case .failure(let error):
                    print(error)
                }}
        }
        self.wait({!self.isGet}) {
            
            print(self.getBearerToken())
        }
        return bearerToken
    }
    
    
    //---ObjectDetection処理
    func UPLOD(bearerToken : String)
    {
        //Parameter HERE
        let parameters:[String: Any] = [
            "file": "swift_file.jpeg",
            "modelName":"object_detection-wally_work_y_10-10000-1",
            "modelVersion":"1"
        ]
        
        //Header HERE
        let headers = [
            "content-type": "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
            "Authorization":self.getBearerToken(),
            "cache-control": "no-cache"
        ]
        
//        let image = UIImage(named: "43_10")
        let imageData = image!.jpegData(compressionQuality: 1)
        
        struct objectDetectionResult: Codable {
            let detection_boxes: [[Double]]?
            let detection_classes:[String]?
            let detection_scores:[Double]?
            let num_detections:[Int]?
            
        }
        struct Results : Codable{
            let results : [objectDetectionResult]
        }
        var results : Results?
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            //Parameter for Upload files
            multipartFormData.append(imageData!, withName: "file",fileName: "swift_file.jpeg" , mimeType: "image/png")
            
            // Send other request parameters
            for (key, value) in parameters {
                multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
            }
            
        }, usingThreshold:UInt64.init(),
           to: "https://mlfproduction-retrain-od-inference.cfapps.eu10.hana.ondemand.com/api/v2alpha1/image/object-detection/", //URL Here
            method: .post,
            headers: headers, //pass header dictionary here
            encodingCompletion: { (result) in
                
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        guard let json = response.data else{
                            return
                        }
                        self.isGet2 = true
                        // JSONパース
                        self.wait({!self.isGet2}){
                            do {
                            let jsonDecoded = try! JSONDecoder().decode(objectDetectionResult.self, from:json)
                            self.isGet3 = true
                            self.wait({!self.isGet3}){
                                 let json:JSON = JSON(response.result.value ?? kill)
                                print(json)
                                
                     
                                
                                
                                print(jsonDecoded)
                                 activityIndicatorView.stopAnimating()
                               
                                //変換したデータをセット
                                self.setDetectionBoxes(detectionBoxesP: jsonDecoded.detection_boxes!)
                                self.setClassNames(classNamesP: jsonDecoded.detection_classes!)
                                self.setDetectionScores(detectionScoresP: jsonDecoded.detection_scores!)
                                self.setNum_detections(num_detectionsP: jsonDecoded.num_detections![0])
                                
                                if(self.getNum_detections() == 0){
                                     let deg = 90.0
                                    let title = "ウォーリーを探せませんでした。"
                                    let message = "「戻る」を押してもう一度実行してください。"
                                    let okText = "ok"
                                    
                                    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
                                    let okayButton = UIAlertAction(title: okText, style: UIAlertAction.Style.cancel, handler: nil)
                                    alert.addAction(okayButton)
                                    
                                    self.present(alert, animated: true, completion: nil)
                                }
                                    // エラー処理
                                }
                                //テスト動作用
                                
                            }catch{
                                
                                
                                
                                let title = "ウォーリーを探せませんでした。"
                                let message = "「戻る」を押してもう一度実行してください。"
                                let okText = "ok"
                                
                                let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
                                let okayButton = UIAlertAction(title: okText, style: UIAlertAction.Style.cancel, handler: nil)
                                alert.addAction(okayButton)
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    break
                case .failure(let encodingError):
                    print("the error is  : \(encodingError.localizedDescription)")
                    break
                }
        })
    }
    
    
    //Rectangle描画処理
    func drawRectangle() {
//        screenWidthG = Double(self.view.bounds.width)
//        screenHeightG = Double(self.view.bounds.height)
        screenWidthG = Double(375)
        screenHeightG = Double(554)
        
        
        let testDraw = Draw(frame: CGRect(x: 0, y: 129,
                                          width: screenWidthG, height: screenHeightG))
        // 透明にする
        testDraw.isOpaque = false
       
        self.view.addSubview(testDraw)
    }
    
    
    
    
    
    //---setter,getter
    private func setBearerToken(bearerToken: String){
        self.bearerToken = bearerToken
        isGet = true
    }
    
    private func getBearerToken() -> String{
        return self.bearerToken
    }
    
    private func setDetectionBoxes(detectionBoxesP: [[Double]]){
        detectionBoxes = detectionBoxesP
        isGet4 =  true
    }
    
    private func getDetectionBoxes() -> [[Double]]{
        return detectionBoxes
    }
    
    private func setClassNames(classNamesP: [String]){
        classNames = classNamesP
        isGet4 = true
    }
    
    private func getClassNames() -> [String]{
        return classNames
    }
    
    private func setDetectionScores(detectionScoresP: [Double]){
        detectionScores = detectionScoresP
        isGet4 = true
    }
    
    private func getDetectionScores() -> [Double]{
        return detectionScores
    }
    
    private func setNum_detections(num_detectionsP: Int){
        num_detections = num_detectionsP
        isGet4 =  true
    }
    
    private func getNum_detections() -> Int{
        return num_detections
    }
    
    
    func resize(image: UIImage, width: Double) -> UIImage {
        let aspectScale = image.size.height / image.size.width
        let resizedSize = CGSize(width: width, height: width * Double(aspectScale))
        UIGraphicsBeginImageContext(resizedSize)
        image.draw(in: CGRect(x: 0, y: 0, width: resizedSize.width, height: resizedSize.height))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resizedImage!
        
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func styleCaptureButton() {
        let deg = 90.0
        Button.transform =   CGAffineTransform(rotationAngle: CGFloat(M_PI*deg/180.0))
    }
    
    
    //---非同期処理
    private func wait(_ waitContinuation: @escaping (() -> Bool), compleation: @escaping (() -> Void)) {
        
        var wait = waitContinuation()
        
        // Wait for the wait condition to be cleared at 0.01 second interval.
        
        let semaphore = DispatchSemaphore(value: 0)
        DispatchQueue.global().async {
            while wait {
                DispatchQueue.main.async {
                    wait = waitContinuation()
                    semaphore.signal()
                }
                
                semaphore.wait()
                Thread.sleep(forTimeInterval: 0.01)
            }
            
            DispatchQueue.main.async {
                compleation()
            }
        }
    }
}



