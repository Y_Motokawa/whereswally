//
//  Draw.swift
//  ObjectDetectionCamera
//
//  Created by yuta motokawa on 2019/03/17.
//  Copyright © 2019 Apoorv Mote. All rights reserved.
//


import UIKit

import Foundation

class Draw: UIView {
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        
        let line = UIBezierPath();
        
        // 青色の長方形のShapeNodeを作成.
        for i in 0..<num_detections {
            
            print(detectionBoxes[i][0])
            // get the box attributes
            
            var left = detectionBoxes[i][1] * screenWidthG
            var top  = detectionBoxes[i][0] * screenHeightG
            var width  = (detectionBoxes[i][3] * screenWidthG  - detectionBoxes[i][1] * screenWidthG  );
            var height = (detectionBoxes[i][2] * screenHeightG - detectionBoxes[i][0] * screenHeightG );
         
            
            
//            var top2  = detectionBoxes[i][1] * screenHeightG
//            var width2  = (detectionBoxes[i][3] * screenHeightG - detectionBoxes[i][1] * screenHeightG);
//            var left2 = screenWidthG -  detectionBoxes[i][0] * screenWidthG - 40
//            var height2 = (detectionBoxes[i][2] * screenWidthG  - detectionBoxes[i][0] * screenWidthG  );
//
            
             let rectangle = UIBezierPath(rect: CGRect(x: left,y: top,width: width,height: height))
            
//            let rectangle2 = UIBezierPath(rect: CGRect(x: left2,y: top2,width: width2,height: height2))
//
            
            //        var text = "#" + num  + ": " + classNames[i] + " " + (detectionScores * 100).toFixed(2) + "%"
            //        rectangle.
            let label = UILabel()
            
            let text : String = classNames[i] + ":" + String(Int(detectionScores[i]*100)) + "%"
            let deg = 90.0
            label.text = text
            label.frame = CGRect(x: left, y: top, width:  100,height: 20)
            label.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI*deg/180.0))
            label.sizeToFit()
            
            
            // 色の設定
            if(detectionScores[i]<=Double(1) &&  Double(0.8)<detectionScores[i]){
                UIColor.green.setStroke()
                label.backgroundColor = UIColor.green
            }else  if(detectionScores[i]<=Double(0.8) &&  Double(0.6)<detectionScores[i]){
                UIColor.yellow.setStroke()
                label.backgroundColor = UIColor.yellow
            }else  if(detectionScores[i]<=Double(0.6) &&  Double(0.4)<detectionScores[i]){
                UIColor.red.setStroke()
                label.backgroundColor = UIColor.red
            }else if(detectionScores[i]<=Double(0.4)){
                UIColor.black.setStroke()
                label.backgroundColor = UIColor.black
                label.textColor = UIColor.white
            }
            // ライン幅
            rectangle.lineWidth = 1
//            rectangle2.lineWidth = 1
            // 描画
            rectangle.stroke()
//             rectangle2.stroke()
            self.addSubview(label)
           
            
        }
        
    }
}


